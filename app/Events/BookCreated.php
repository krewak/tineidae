<?php

namespace Tineidae\Events;

use Tineidae\Models\Book;

class BookCreated {

	public function __construct(Book $book) {
		$book->read()->create(["book_uuid" => $book->uuid]);
	}

}
