<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tineidae\Interfaces\Repository;

class LanguageBooksController extends ListController {

	protected $slug;

	protected function getSearchableColumns(): array {
		return ["name" => "languages.name", "slug" => "languages.slug"];
	}

	protected function getSortableColumns(): array {
		return [
			"name" => "languages.name",
			"authorsCount" => "authors_count",
			"booksCount" => "books_count",
			"readsCount" => "reads_count"
		];
	}

	public function get(Request $request, string $slug = ""): JsonResponse {
		$this->slug = $slug;
		return parent::get($request);
	}

	protected function customizeRepository(Repository $repository): Repository {
		return $repository->addMasterFilter("languages.slug", $this->slug);
	}

}
