<?php

namespace Tineidae\Events;

use Tineidae\Models\Book;
use Tineidae\Models\Tag;

class BookTagged {

	public function __construct(Book $book, array $pivotIds) {
		foreach($pivotIds as $pivotId) {
			$tag = Tag::find($pivotId);

			if(is_null($tag->last_read_book_uuid)) {
				$tag->last_read_book_uuid = $book->uuid;
				$tag->save();
			} else {
				$tag->lastBook->read->readAt;
			}
		}
	}

}
