<?php

namespace Tineidae\Models\Abstracts;

use Ramsey\Uuid\Uuid;
use Tineidae\Events\GenerateModelUuid;
use Tineidae\Interfaces\IdentifiableByUUID;

/**
 * @property string uuid
 */
abstract class UuidModel extends BaseModel implements IdentifiableByUUID {

	public $incrementing = false;
	protected $primaryKey = "uuid";
	protected $hidden = ["uuid", "created_at", "updated_at"];

	public static function boot(): void {
		parent::boot();

		static::creating(function(IdentifiableByUUID $model): void {
			event(new GenerateModelUuid($model));
		});
	}

	public function getUuidAttribute(): string {
		$uuid = $this->attributes["uuid"];
		return is_a($uuid, Uuid::class) ? $uuid->toString() : $uuid;
	}

}
