<?php

namespace Tineidae\Events;

use Tineidae\Interfaces\StandardSystemModel;

class StandardSystemModelCreated {

	public function __construct(StandardSystemModel $model) {
		event(new GenerateModelUuid($model));
		event(new GenerateModelSlug($model));
	}

}