<?php

namespace Tineidae\Repositories\Lists;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Repositories\Tag as BaseTagRepository;

class Tag extends BaseTagRepository {

	protected function extendQuery(Builder $query): Builder {
		return $query
			->leftJoin("books", "books.uuid", "=", "tags.last_read_book_uuid")
			->leftJoin("reads", "books.uuid", "=", "reads.book_uuid")
			->with(["book", "book.read"])
			->withCount("books")
			->orderBy("tags.name");
	}

	protected function map(Arrayable $model): array {
		/** @var \Tineidae\Models\Tag $model */
		$array = $model->toArray();

		$array["books"] = $model["booksCount"];
		$array["lastRead"] = is_null($model->book) ? null : [
			"readAt" => $model->book->read->readAt->polishDate(),
			"readAgo" => $model->book->read->readAt->diffForHumans(),
			"slug" => $model->book->slug,
			"title" => $model->book->title,
		];

		unset($array["book"]);
		unset($array["lastReadBookUuid"]);
		unset($array["booksCount"]);

		return $array;
	}

}