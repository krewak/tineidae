<?php

namespace Tineidae\Repositories\Lists;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Repositories\Author as BaseAuthorRepository;

class Author extends BaseAuthorRepository {

	protected function extendQuery(Builder $query): Builder {
		return $query
			->join("languages", "languages.uuid", "=", "authors.language_uuid")
			->join("reads", "reads.book_uuid", "=", "authors.last_read_book_uuid")
			->with(["language", "book", "book.read"])
			->withCount("books")
			->orderBy("last_name")
			->orderBy("first_name");
	}

	protected function map(Arrayable $model): array {
		/** @var \Tineidae\Models\Author $model */
		$array = $model->toArray();

		$array["language"] = $model->language;
		$array["lastBook"] = $model->book ? [
			"slug" => $model->book->slug,
			"title" => $model->book->title,
			"read_at" => $model->book->read->readAt ? $model->book->read->readAt->diffForHumans() : null,
		] : [];

		unset($array["altName"]);
		unset($array["birthYear"]);
		unset($array["deathYear"]);
		unset($array["lastReadBookUuid"]);

		return $array;
	}

}
