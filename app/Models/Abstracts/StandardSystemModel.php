<?php

namespace Tineidae\Models\Abstracts;

use Illuminate\Contracts\Support\Arrayable;
use Tineidae\Events\GenerateModelSlug;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Interfaces\StandardSystemModel as StandardSystemModelInterface;

/**
 * @property string slug
 */
abstract class StandardSystemModel extends UuidModel implements StandardSystemModelInterface {

	public static function boot(): void {
		parent::boot();

		static::creating(function(AccessibleBySlug $model): void {
			event(new GenerateModelSlug($model));
		});
	}

	public function getBySlug(string $slug): Arrayable {
		return self::where("slug", $slug)->firstOrFail();
	}

}
