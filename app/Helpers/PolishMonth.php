<?php

namespace Tineidae\Helpers;

use Exception;

class PolishMonth {

	const MONTHS = [
		"stycznia",
		"lutego",
		"marca",
		"kwietnia",
		"maja",
		"czerwca",
		"lipca",
		"sierpnia",
		"września",
		"października",
		"listopada",
		"grudnia",
	];

	public static function get(int $number): string {
		try {
			return self::MONTHS[$number - 1];
		} catch(Exception $exception) {
			return "";
		}
	}

}
