<?php

namespace Tineidae\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tineidae\Interfaces\APIResponse;

abstract class Middleware {

	/**
	 * @var APIResponse
	 */
	protected $response;

	public function __construct(APIResponse $response) {
		$this->response = $response;
	}

	abstract public function handle(Request $request, Closure $next, $guard = null);

}
