<?php

namespace Tineidae\Tests\Facade;

use Tineidae\Interfaces\StandardSystemModel;
use Tineidae\Tests\TestCase;

abstract class AccessTest extends TestCase {

	/** @var bool $extendTest */
	protected $extendTest = false;

	abstract protected function getAccessUrl(): string;
	abstract protected function createEntry(): StandardSystemModel;

	protected function extendActions(StandardSystemModel $entry): void {

	}

	public function testAccess(): void {
		$this->resetDatabase();

		$this->internalTestEmptyListing();
		$this->internalTestNonExistingEntry("non-existing-entry");

		$entry = $this->createEntry();
		$this->internalTestExistingEntry($entry->slug);

		if($this->extendTest) {
			$this->extendActions($entry);
			$this->internalTestExistingEntry($entry->slug);
		}
	}

	public function internalTestEmptyListing(): void {
		$response = $this->get($this->getAccessUrl())->getResponseObject();
		$this->assertEmpty($response->data);
	}

	public function internalTestNonExistingEntry(string $slug): void {
		$response = $this->get($this->getAccessUrl() . $slug)->getResponseObject();
		$this->assertFalse($response->status);
		$this->assertEmpty($response->data);
	}

	public function internalTestExistingEntry(string $slug): void {
		$response = $this->get($this->getAccessUrl() . $slug)->getResponseObject();
		$this->assertTrue($response->status);
		$this->assertNotEmpty($response->data);
	}

}
