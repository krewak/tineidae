<?php

namespace Tineidae\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Guest extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if($request["token"] && !$request["exception"]) {
			$this->response->pushMessage("Area restricted for guests.");
			return response()->json($this->response->get(), 401);
		}

		return $next($request);
	}

}
