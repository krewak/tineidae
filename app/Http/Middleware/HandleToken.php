<?php

namespace Tineidae\Http\Middleware;

use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Tineidae\Models\User;
use Throwable;

class HandleToken extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		$request["token"] = $this->retrieveToken($request);
		$request["auth"] = null;
		$request["exception"] = null;

		if($request["token"]) {
			try {
				$credentials = JWT::decode($request["token"], env("JWT_SECRET"), [env("JWT_ALGORITHM")]);
				$request["auth"] = User::find($credentials->sub);
			} catch(ExpiredException $exception) {
				$request["exception"] = $exception;
			} catch(Throwable $exception) {
				$request["exception"] = $exception;
			}
		}

		return $next($request);
	}

	protected function retrieveToken(Request $request): string {
		$token = $request->header("Authorization") ?? "";
		return str_replace("Bearer ", "", $token);
	}

}
