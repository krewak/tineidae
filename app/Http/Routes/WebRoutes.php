<?php

namespace Tineidae\Http\Routes;

use Laravel\Lumen\Routing\Router;

class WebRoutes extends Routes {

	public function __construct(Router $router) {
		$router->get("/", ["as" => "welcome", "uses" => "StatusController@getStatus"]);
		$router->get("/status", ["as" => "status", "uses" => "StatusController@getStatus"]);

		$router->get("/dashboard/recent", ["as" => "dashboard.recent", "uses" => "HomeController@getRecentRead"]);
		$router->get("/dashboard/recent/{number}", ["as" => "dashboard.recent.limited", "uses" => "HomeController@getRecentReads"]);
		$router->get("/dashboard/trend/books", ["as" => "dashboard.trends.books", "uses" => "HomeController@getBookTrend"]);
		$router->get("/dashboard/trend/pages", ["as" => "dashboard.trends.pages", "uses" => "HomeController@getPageTrend"]);

		$router->get("/search", ["as" => "search", "uses" => "SearchController@getResults"]);

		$namespace = "RepositoryControllers\\Lists";
		$router->group(["namespace" => $namespace], function() use($router): void {
			$router->get("/books", ["as" => "book.list", "uses" => "BooksController@get"]);
			$router->get("/languages", ["as" => "language.list", "uses" => "LanguagesController@get"]);
			$router->get("/devices", ["as" => "device.list", "uses" => "DevicesController@get"]);
			$router->get("/authors", ["as" => "author.list", "uses" => "AuthorsController@get"]);
			$router->get("/tags", ["as" => "tag.list", "uses" => "TagsController@get"]);

			$router->get("/books/of/language/{slug}", ["as" => "language.books.list", "uses" => "LanguageBooksController@get"]);
		});

		$namespace = "RepositoryControllers\\Entries";
		$router->group(["namespace" => $namespace], function() use($router): void {
			$router->get("/languages/{slug}", ["as" => "language.entry", "uses" => "LanguageController@get"]);
			$router->get("/devices/{slug}", ["as" => "device.entry", "uses" => "DeviceController@get"]);
			$router->get("/authors/{slug}", ["as" => "author.entry", "uses" => "AuthorController@get"]);
			$router->get("/tags/{slug}", ["as" => "tag.entry", "uses" => "TagController@get"]);
		});
	}

}

