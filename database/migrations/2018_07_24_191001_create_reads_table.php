<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::create("reads", function(Blueprint $table) {
			$table->string("book_uuid")->unique();
			$table->foreign("book_uuid")->references("uuid")->on("books");

			$table->date("started_at")->nullable()->default(null);
			$table->date("read_at")->nullable()->default(null);

			$table->boolean("repeated")->default(false);

			$table->string("language_uuid")->nullable()->default(null);
			$table->foreign("language_uuid")->references("uuid")->on("languages");

			$table->string("device_uuid")->nullable()->default(null);
			$table->foreign("device_uuid")->references("uuid")->on("devices");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("reads", function(Blueprint $table) {
			$table->dropForeign(["book_uuid"]);
			$table->dropForeign(["language_uuid"]);
			$table->dropForeign(["device_uuid"]);
		});
		Schema::dropIfExists("reads");
	}
}
