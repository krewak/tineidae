<?php

namespace Tineidae\Http\Controllers;

use Illuminate\Http\JsonResponse;

class StatusController extends Controller {

	public function getStatus(): JsonResponse {
		$this->response->setStatus(true)
			->pushMessage("App is working.");

		return $this->getResponse();
	}

}
