<?php

namespace Tineidae\Tests\Facade;

use Tineidae\Interfaces\StandardSystemModel;
use Tineidae\Models\Device;

class DeviceAccessTest extends AccessTest {

	protected function createEntry(): StandardSystemModel {
		return Device::create([
			"name" => "Kindle Paperwhite 3",
		]);
	}

	protected function getAccessUrl(): string {
		return "/api/devices/";
	}

}
