<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

class AuthorsController extends ListController {

	protected function getSearchableColumns(): array {
		return [
			"first_name" => "authors.first_name",
			"last_name" => "authors.last_name",
			"slug" => "authors.slug",
		];
	}

	protected function getSortableColumns(): array {
		return [
			"name" => "authors.last_name",
			"language" => "languages.name",
			"gender" => "authors.gender",
			"books" => "books_count",
			"read" => "reads.read_at",
		];
	}

}
