<?php

namespace Tineidae\Tests;

use Tineidae\Helpers\APIResponse;

class ApplicationStatusTest extends TestCase {

	public function testApplicationStatusResponseFormat(): void {
		$responseTemplate = new APIResponse();
		$responseTemplate = $responseTemplate->get();

		$response = $this->get("/api/")->getResponseContent();

		$this->assertEquals(sizeof($responseTemplate), sizeof($response));
		foreach($responseTemplate as $key => $value) {
			$this->assertArrayHasKey($key, $response);
		}
	}

}
