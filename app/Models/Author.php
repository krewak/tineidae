<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tineidae\Helpers\Gender;
use Tineidae\Models\Abstracts\StandardSystemModel;

/**
 * @property string $name
 * @property string $firstName
 * @property string $lastName
 * @property string $altName
 * @property Gender $gender
 * @property int|null $birthYear
 * @property int|null $deathYear
 * @property Language $language
 * @property string $language_uuid
 * @property string $portrait
 * @property string|null $last_read_book_uuid
 * @property Book|null $lastBook
 */
class Author extends StandardSystemModel {

	protected $fillable = ["first_name", "last_name", "alt_name", "gender", "birth_year", "death_year", "language_uuid", "portrait"];
	protected $hidden = ["uuid", "language_uuid", "created_at", "updated_at"];

	public function getSlugBase(): string {
		return $this->name;
	}

	public function getGenderAttribute(): Gender {
		return new Gender($this->getOriginal("gender"));
	}

	public function getNameAttribute(): string {
		return $this->firstName ." ". $this->lastName;
	}

	public function language(): BelongsTo {
		return $this->belongsTo(Language::class);
	}

	public function books(): BelongsToMany {
		return $this->belongsToMany(Book::class, "book_authors");
	}

	public function book(): BelongsTo {
		return $this->belongsTo(Book::class, "last_read_book_uuid", "uuid");
	}

}
