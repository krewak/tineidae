<?php

namespace Tineidae\Repositories;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Book;

class LanguageBook extends ModelRepository {

	protected function extendQuery(Builder $query): Builder {
		return $query->selectRaw("books.*, languages.slug")
			->join("reads", "reads.book_uuid", "=", "books.uuid")
			->join("languages", "languages.uuid", "=", "books.language_uuid")
			->orderBy("read_at", "desc");
	}

	protected function map(Arrayable $model): array {
		/** @var Book $model */
		$array = $model->toArray();
		return $array;
	}

	protected function getDataSource(): AccessibleBySlug {
		return new Book();
	}

}