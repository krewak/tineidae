<?php

namespace Tineidae\Services;

use Illuminate\Support\Carbon;
use Tineidae\Models\Read;

class DashboardPageTrendService extends DashboardTrendService {

	public function getValues(): array {
		return array_map(function(Carbon $month): string {
			return Read::select("reads.read_at, books.pages")
				->join("books", "books.uuid", "=", "reads.book_uuid")
				->whereMonth("read_at", $month->format("m"))
				->whereYear("read_at", $month->format("Y"))
				->sum("pages");
		}, $this->getMonthsArray());
	}

}
