import Vue from "vue"
import VueCookie from "vue-cookie"

import axios from "axios"
import router from "./router.js"
import store from "./store.js"

import "semantic-ui-css/semantic.min.css"

import App from "./components/App"
import Loader from "./components/Layout/Components/Loader"

axios.defaults.baseURL = "/api/"

axios.interceptors.request.use(request => {
	request.headers.common["Authorization"] = "Bearer " + VueCookie.get("tineidae-jwt-token")
	return request
})

axios.interceptors.response.use(response => {
	if(response.data.token) {
		VueCookie.set("tineidae-jwt-token", response.data.token)
	}
	return response
})

Vue.prototype.$http = axios
Vue.component("loader", Loader)

let titleSeparator = " :: "
let titleApplicationName = "Tineidae"

axios.get("status").then(response => {
	Vue.prototype.$auth = response.data.user
	Vue.mixin({
		methods: {
			setPageTitle: function(title) {

				if(Array.isArray(title)) {
					title.push(titleApplicationName)
					return window.document.title = title.join(titleSeparator)
				}

				return window.document.title = title + titleSeparator + titleApplicationName
			}
		}
	});

	new Vue({
		el: "#app",
		router,
		store,
		components: { App },
		template: "<App></App>"
	})
})
