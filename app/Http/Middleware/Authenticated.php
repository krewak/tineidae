<?php

namespace Tineidae\Http\Middleware;

use Closure;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;

class Authenticated extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if(!$request["token"]) {
			$this->response->pushMessage("Token not provided.");
			return response()->json($this->response->get(), 401);
		}

		if($request["exception"]) {
			if($request["exception"] instanceof ExpiredException) {
				$this->response->pushMessage("Provided token is expired.");
				return response()->json($this->response->get(), 400);
			}

			$this->response->pushMessage("An error while decoding token.");
			return response()->json($this->response->get(), 400);
		}

		return $next($request);
	}

}
