<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

class TagsController extends ListController {

	protected function getSearchableColumns(): array {
		return ["name" => "tags.name", "slug" => "tags.slug"];
	}

	protected function getSortableColumns(): array {
		return [
			"name" => "tags.name",
			"count" => "books_count",
			"title" => "books.title",
			"read_at" => "reads.read_at"
		];
	}

}
