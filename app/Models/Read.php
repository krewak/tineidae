<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Tineidae\Models\Abstracts\BaseModel;

/**
 * @property \Carbon\Carbon|null $started_at
 * @property \Carbon\Carbon|null $read_at
 * @property boolean $repeated
 * @property string $language_uuid
 * @property Language $language
 * @property string $device_uuid
 * @property Device $device
 */
class Read extends BaseModel {

	protected $dates = ["started_at", "read_at", "created_at", "updated_at"];
	protected $fillable = ["book_uuid", "started_at", "read_at", "repeated", "language_uuid", "device_uuid"];
	protected $primaryKey = "book_uuid";
	public $incrementing = false;

	public function book(): HasOne {
		return $this->hasOne(Book::class, "uuid");
	}

	public function language(): BelongsTo {
		return $this->belongsTo(Language::class);
	}

	public function device(): BelongsTo {
		return $this->belongsTo(Device::class);
	}

}
