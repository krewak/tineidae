<?php

namespace Tineidae\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController extends Controller {

	public function getResults(Request $request): JsonResponse {
		$searchedPhrase = $request->get("searchPhrase");
		if($searchedPhrase) {
			$this->response->setData([
				["label" => "angielski", "repository" => "language"],
				["label" => "James Luceno", "repository" => "author"],
				["label" => "Jednocząca Moc", "repository" => "book"],
				["label" => "literatura piękna", "repository" => "tag"],
			]);
		}

		return $this->getResponse();
	}

}
