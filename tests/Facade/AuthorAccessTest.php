<?php

namespace Tineidae\Tests\Facade;

use Tineidae\Interfaces\StandardSystemModel;
use Tineidae\Models\Author;
use Tineidae\Models\Language;

class AuthorAccessTest extends AccessTest {

	protected function createEntry(): StandardSystemModel {
		$language = Language::create([
			"name" => "polski",
			"symbol" => "pl",
			"abbreviation" => "pol.",
		]);

		return Author::create([
			"first_name" => "James",
			"last_name" => "Luceno",
			"gender" => "M",
			"birth_year" => "1947",
			"language_uuid" => $language->uuid,
		]);
	}

	protected function getAccessUrl(): string {
		return "/api/authors/";
	}

}
