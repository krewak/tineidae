<?php

use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Tineidae\Helpers\Gender;
use Tineidae\Models\{Author, Book, Language, Read, Tag, User};

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(User::class, function(Generator $faker): array {
	return [
		"email" => $faker->safeEmail,
		"password" => Hash::make("secret"),
		"name" => $faker->firstName,
	];
});

$factory->define(Language::class, function(Generator $faker): array {
	$countryCode = strtolower($faker->countryCode);

	return [
		"name" => $faker->word,
		"symbol" => $countryCode,
		"abbreviation" => $countryCode . ".",
	];
});

$factory->define(Author::class, function(Generator $faker): array {
	return [
		"first_name" => $faker->firstName,
		"last_name" => $faker->lastName,
		"gender" => $faker->randomElement(Gender::values),
		"birth_year" => $faker->year("now"),
		"death_year" => $faker->optional(0.5, null)->year("now"),
		"portrait" => $faker->imageUrl(320, 500),
	];
});

$factory->define(Tag::class, function(Generator $faker): array {
	return [
		"name" => $faker->word,
	];
});

$factory->define(Book::class, function(Generator $faker): array {
	return [
		"title" => $faker->sentence(3, true),
		"original_title" => $faker->sentence(3, true),
		"publication_year" => $faker->year("now"),
		"pages" => $faker->biasedNumberBetween(220, 720),
		"cover" => $faker->imageUrl(320, 500),
		"summary" => $faker->text
	];
});

$factory->define(Read::class, function(Generator $faker): array {
	$startedAt = $faker->dateTime;

	return [
		"repeated" => $faker->boolean(15),
		"started_at" => $startedAt,
		"read_at" => Carbon::parse($startedAt->format("Y-m-d H:i:s"))->addDays($faker->numberBetween(1, 20))
	];
});
