<?php

namespace Tineidae\Events;

use Tineidae\Models\Author;
use Tineidae\Models\Book;

class AuthorAssignedToBook {

	public function __construct(Book $book, array $pivotIds) {
		foreach($pivotIds as $pivotId) {
			$author = Author::find($pivotId);
			$author->last_read_book_uuid = $book->uuid;
			$author->save();
		}
	}

}
