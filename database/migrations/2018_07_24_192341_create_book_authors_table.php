<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookAuthorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::create("book_authors", function(Blueprint $table) {
			$table->string("book_uuid");
			$table->foreign("book_uuid")->references("uuid")->on("books")->onDelete("cascade");

			$table->string("author_uuid");
			$table->foreign("author_uuid")->references("uuid")->on("authors")->onDelete("cascade");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("book_authors", function(Blueprint $table) {
			$table->dropForeign(["book_uuid"]);
			$table->dropForeign(["author_uuid"]);
		});
		Schema::dropIfExists("book_authors");
	}
}
