<?php

namespace Tineidae\Interfaces;

interface Repository {

	public function all(): array;
	public function get(string $slug = ""): array;
	public function filter(array $columns, string $phrase = ""): Repository;
	public function addMasterFilter(string $column, string $phrase = ""): Repository;
	public function sort(string $column, bool $ascending = true): Repository;

}
