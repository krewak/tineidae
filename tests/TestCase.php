<?php

namespace Tineidae\Tests;

use Illuminate\Contracts\Console\Kernel;
use Laravel\Lumen\Application;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use stdClass;

abstract class TestCase extends BaseTestCase {

	public function createApplication(): Application {
		return require __DIR__ . "/../app/bootstrap.php";
	}

	public function getResponseContent(): array {
		return (array) $this->getResponseObject();
	}

	public function getResponseObject(): stdClass {
		return json_decode($this->response->getContent());
	}

	public function resetDatabase(): void {
		app(Kernel::class)->call("migrate:fresh");
		echo "Database refreshed." . PHP_EOL;
	}

}