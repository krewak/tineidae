<?php

namespace Tineidae\Interfaces;

interface Authenticatable {

	public function getID(): string;

}
