<?php

namespace Tineidae\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Routing\Router;
use Tineidae\Http\Routes\AuthRoutes;
use Tineidae\Http\Routes\GuestRoutes;
use Tineidae\Http\Routes\WebRoutes;
use Tineidae\Interfaces\ApplicationRoutes;

class RouterServiceProvider extends ServiceProvider {

	protected $namespace = "Tineidae\Http\Controllers";

	public function boot(): void {
		/** @var Router $router */
		$router = $this->app["router"];

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token",
			"prefix" => "api"
		], function(Router $router): ApplicationRoutes {
			return new WebRoutes($router);
		});

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token|guest",
			"prefix" => "api"
		], function(Router $router): ApplicationRoutes {
			return new GuestRoutes($router);
		});

		$router->group([
			"namespace" => $this->namespace,
			"middleware" => "token|auth|refresh",
			"prefix" => "api"
		], function(Router $router): ApplicationRoutes {
			return new AuthRoutes($router);
		});
	}

}
