<?php

namespace Tineidae\Services;

use Tineidae\Models\Book;

class DashboardRecentReadDataService {

	private $numberOfBooks = null;

	public function getData(): array {
		$query = Book::join("reads", "reads.book_uuid", "=", "books.uuid")
			->with(["authors", "device"])
			->orderByRaw("reads.read_at IS NOT NULL, reads.read_at DESC, books.created_at DESC");

		if(is_null($this->numberOfBooks)) {
			$query = $query->first();
		} else {
			$query = $query->limit($this->numberOfBooks + 1)->get();
		}

		return $query->toArray();
	}

	public function setNumberOfBooks(int $number): self {
		$this->numberOfBooks = $number;
		return $this;
	}

}
