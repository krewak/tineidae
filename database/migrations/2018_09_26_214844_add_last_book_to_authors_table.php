<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastBookToAuthorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::table("authors", function(Blueprint $table) {
			$table->string("last_read_book_uuid")->nullable()->default(null)->after("language_uuid");
			$table->foreign("last_read_book_uuid")->references("uuid")->on("books")->onDelete("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("authors", function(Blueprint $table) {
			$table->dropForeign(["last_read_book_uuid"]);
		});
	}
}
