<?php

namespace Tineidae\Tests\Facade;

use Tineidae\Interfaces\StandardSystemModel;
use Tineidae\Models\Language;

class LanguageAccessTest extends AccessTest {

	protected $extendTest = true;

	protected function getAccessUrl(): string {
		return "/api/languages/";
	}

	protected function createEntry(): StandardSystemModel {
		return Language::create([
			"name" => "polski",
			"symbol" => "pl",
			"abbreviation" => "pol.",
		]);
	}

	protected function extendActions(StandardSystemModel $entry): void {
		/** @var Language $entry */
		$entry->authors()->create([
			"first_name" => "James",
			"last_name" => "Luceno",
			"gender" => "M",
			"birth_year" => "1947",
			"language_uuid" => $entry->uuid
		]);
	}

}
