<?php

namespace Tineidae\Interfaces;

interface APIResponse {

	public function get(): array;
	public function setStatus(bool $status): APIResponse;
	public function pushMessage(string $message): APIResponse;
	public function setData(array $data): APIResponse;
	public function setUser(Authenticatable $user): APIResponse;
	public function setToken(string $token): APIResponse;

}
