<?php

namespace Tineidae\Models;

use Tineidae\Models\Abstracts\StandardSystemModel;

/**
 * @property string name
 */
class Device extends StandardSystemModel {

	protected $fillable = ["name"];

	public function getSlugBase(): string {
		return $this->name;
	}

}
