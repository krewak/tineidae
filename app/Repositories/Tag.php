<?php

namespace Tineidae\Repositories;

use Illuminate\Contracts\Support\Arrayable;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Book;
use Tineidae\Models\Tag as TagModel;

class Tag extends ModelRepository {

	protected function map(Arrayable $model): array {
		/** @var TagModel $model */
		$array = $model->toArray();
		$array["books"] = $model->books->map(function(Book $model): array {
			$array = [];

			$array["title"] = $model->title;
			$array["slug"] = $model->slug;
			$array["cover"] = $model->cover;
			$array["pages"] = $model->pages;

			$array["original"] = [
				"title" => $model->original_title,
				"publication" => $model->publication_year,
			];

			$array["reading"] = [
				"language" => [
					"slug" => $model->read->language->slug,
					"symbol" => $model->read->language->symbol,
					"name" => $model->read->language->name,
				],
				"publication" => $model->publication_year,
				"when" => $model->read->readAt->polishDate(),
				"readAgo" => $model->read->readAt->diffForHumans(),
			];

			return $array;
		});

		unset($array["lastReadBookUuid"]);

		return $array;
	}

	protected function getDataSource(): AccessibleBySlug {
		return new TagModel();
	}

}