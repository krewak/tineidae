<?php

namespace Tineidae\Helpers;

use Carbon\Carbon;
use Tineidae\Interfaces\APIResponse as APIResponseInterface;
use Tineidae\Interfaces\Authenticatable;

class APIResponse implements APIResponseInterface {

	protected $status = false;
	protected $messages = [];
	protected $data = [];
	protected $timestamp = null;
	protected $token = null;
	protected $user = null;
	protected $version = null;

	public function __construct() {
		$this->timestamp = Carbon::now()->toDateTimeString();
		$this->version = getenv("API_VERSION");
	}

	public function get(): array {
		return [
			"status" => $this->status,
			"messages" => $this->messages,
			"data" => $this->data,
			"timestamp" => $this->timestamp,
			"token" => $this->token,
			"user" => $this->user,
			"version" => $this->version,
		];
	}

	public function setStatus(bool $status): APIResponseInterface {
		$this->status = $status;
		return $this;
	}

	public function pushMessage(string $message): APIResponseInterface {
		$this->messages[] = $message;
		return $this;
	}

	public function setData(array $data): APIResponseInterface {
		$this->data = $data;
		return $this;
	}

	public function setUser(Authenticatable $user): APIResponseInterface {
		$this->user = $user;
		return $this;
	}

	public function setToken(string $token): APIResponseInterface {
		$this->token = $token;
		return $this;
	}

}
