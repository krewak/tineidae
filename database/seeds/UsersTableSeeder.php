<?php

use Illuminate\Database\Seeder;
use Tineidae\Models\User;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(): void {
		if(User::count() === 0) {
			factory(User::class, 1)->create();
		}
	}

}
