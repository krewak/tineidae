<?php

namespace Tineidae\Repositories;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Author as AuthorModel;

class Author extends ModelRepository {

	protected function getDataSource(): AccessibleBySlug {
		return new AuthorModel();
	}

	protected function extendQuery(Builder $query): Builder {
		return $query->orderBy("last_name");
	}

	protected function map(Arrayable $model): array {
		/** @var AuthorModel $model */
		$array = $model->toArray();

		$array["language"] = $model->language;
		$array["dates"] = [
			"birth" => $model->birthYear,
			"death" => $model->deathYear,
		];
		$array["books"] = [];

		unset($array["birthYear"]);
		unset($array["deathYear"]);

		return $array;
	}

}