<?php

namespace Tineidae\Services;

use Firebase\JWT\JWT;
use Tineidae\Interfaces\Authenticatable;

class JSONWebTokenService {

	/**
	 * @param Authenticatable $user
	 * @return string
	 */
	public function encodeJWT(Authenticatable $user): string {
		$payload = [
			"iss" => env("JWT_ISSUER"),
			"sub" => $user->getID(),
			"iat" => time(),
			"exp" => time() + env("JWT_TIMEOUT")
		];

		return JWT::encode($payload, env("JWT_SECRET"));
	}

	/**
	 * @param $token
	 * @return string
	 */
	public function extendTokenExpirationDate($token): string {
		$payload = JWT::decode($token, env("JWT_SECRET"), [env("JWT_ALGORITHM")]);
		$payload->exp = time() + env("JWT_TIMEOUT");

		return JWT::encode($payload, env("JWT_SECRET"));
	}

}