<?php

namespace Tineidae\Repositories;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Language as LanguageModel;
use Tineidae\Models\Author;

class Language extends ModelRepository {

	protected function extendQuery(Builder $query): Builder {
		return $query->orderBy("name");
	}

	protected function map(Arrayable $model): array {
		/** @var LanguageModel $model */
		$array = $model->toArray();
		$array["authors"] = $model->authors->map(function(Author $model): array {
			$array = $model->toArray();
			unset($array["birthYear"]);
			unset($array["deathYear"]);
			unset($array["gender"]);
			return $array;
		});

		return $array;
	}

	protected function getDataSource(): AccessibleBySlug {
		return new LanguageModel();
	}

}