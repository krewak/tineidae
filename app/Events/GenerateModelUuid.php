<?php

namespace Tineidae\Events;

use Illuminate\Support\Str;
use Tineidae\Interfaces\IdentifiableByUUID;

class GenerateModelUuid {

	public function __construct(IdentifiableByUUID $model) {
		$model->uuid = Str::uuid();
	}

}