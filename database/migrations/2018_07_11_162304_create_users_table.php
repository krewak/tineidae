<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::create("users", function(Blueprint $table) {
			$table->string("uuid")->unique();

			$table->string("email")->unique();
			$table->string("name");
			$table->string("password")->nullable();

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::dropIfExists("users");
	}
}
