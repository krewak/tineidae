<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

class LanguagesController extends ListController {

	protected function getSearchableColumns(): array {
		return ["name" => "languages.name", "slug" => "languages.slug"];
	}

	protected function getSortableColumns(): array {
		return [
			"name" => "languages.name",
			"authorsCount" => "authors_count",
			"booksCount" => "books_count",
			"readsCount" => "reads_count"
		];
	}

}
