<?php

use Illuminate\Database\Seeder;
use Tineidae\Models\Author;
use Tineidae\Models\Book;
use Tineidae\Models\Device;
use Tineidae\Models\Language;
use Tineidae\Models\User;

class RealDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(): void {
		$this->seedModelsFromJsonFile("users", User::class);
		$this->seedModelsFromJsonFile("devices", Device::class);
		$this->seedModelsFromJsonFile("languages", Language::class);
		$this->seedModelsFromJsonFile("authors", Author::class, ["language" => Language::class]);
		$this->seedBooksFromJsonFile("books", Book::class);
	}

	protected function seedModelsFromJsonFile(string $repository, string $modelClass, array $relations = []): void {
		$filename = "./database/seeds/data/$repository.json";

		$relatedRelationsUuids = [];
		foreach($relations as $relation => $model) {
			$relatedRelationsUuids[$relation] = [];

			foreach($model::all() as $entity) {
				$relatedRelationsUuids[$relation][$entity->name] = $entity->uuid;
			}
		}

		$file = json_decode(file_get_contents($filename), true);
		foreach($file[$repository] as $jsonData) {
			$model = new $modelClass;
			$model->fill($jsonData);

			foreach($relations as $relation => $value) {
				$fieldName = $relation . "_uuid";
				$model->$fieldName = $relatedRelationsUuids[$relation][$model->$fieldName];
			}

			$model->save();
		}
	}

	protected function seedBooksFromJsonFile(string $repository, string $modelClass): void {
		$filename = "./database/seeds/data/$repository.json";
		$file = json_decode(file_get_contents($filename), true);

		$authorsUuids = [];
		foreach(Author::all() as $author) {
			$authorsUuids[$author->slug] = $author->uuid;
		}

		$devicesUuids = [];
		foreach(Device::all() as $device) {
			$devicesUuids[$device->slug] = $device->uuid;
		}

		$languagesUuids = [];
		foreach(Language::all() as $language) {
			$languagesUuids[$language->slug] = $language->uuid;
		}

		foreach($file[$repository] as $jsonData) {
			/** @var Book $book */
			$book = new $modelClass;
			$book->title = $jsonData["title"];
			$book->original_title = $jsonData["original_title"];
			$book->language_uuid = $languagesUuids[$jsonData["original_language_slug"]];
			$book->publication_year = $jsonData["publication_year"];
			$book->pages = $jsonData["pages"];
			$book->cover = $jsonData["cover"];
			$book->summary = $jsonData["summary"];
			$book->save();

			$read = $book->read;
			$read->started_at = $jsonData["started_at"];
			$read->read_at = $jsonData["read_at"];
			$read->repeated = $jsonData["repeated"];
			$read->language_uuid = $languagesUuids[$jsonData["language_slug"]];
			$read->device_uuid = $devicesUuids[$jsonData["device_slug"]];
			$read->save();

			foreach($jsonData["authors_slugs"] as $authorSlug) {
				$book->authors()->attach($authorsUuids[$authorSlug]);
			}

			echo $book->slug . " created." . PHP_EOL;
		}
	}

}
