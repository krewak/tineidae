<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastBookToTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::table("tags", function(Blueprint $table) {
			$table->string("last_read_book_uuid")->nullable()->default(null)->after("name");
			$table->foreign("last_read_book_uuid")->references("uuid")->on("books")->onDelete("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("tags", function(Blueprint $table) {
			$table->dropForeign(["last_read_book_uuid"]);
		});
	}
}
