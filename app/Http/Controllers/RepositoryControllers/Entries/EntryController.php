<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Entries;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Tineidae\Http\Controllers\RepositoryControllers\RepositoryController;

abstract class EntryController extends RepositoryController {

	/**
	 * @param string $slug
	 * @return JsonResponse
	 */
	public function get(string $slug): JsonResponse {
		try {
			$data = $this->repository->get($slug);
			$this->response->setStatus(true)->setData($data);
		} catch(ModelNotFoundException $exception) {
			$this->response->pushMessage("Model doesn't exist.");
		}

		return $this->getResponse();
	}

}
