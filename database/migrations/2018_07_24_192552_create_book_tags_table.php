<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::create("book_tags", function(Blueprint $table) {
			$table->string("book_uuid");
			$table->foreign("book_uuid")->references("uuid")->on("books")->onDelete("cascade");

			$table->string("tag_uuid");
			$table->foreign("tag_uuid")->references("uuid")->on("tags")->onDelete("cascade");

			$table->unique(["book_uuid", "tag_uuid"]);

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("book_tags", function(Blueprint $table) {
			$table->dropForeign(["book_uuid"]);
			$table->dropForeign(["tag_uuid"]);
		});
		Schema::dropIfExists("book_tags");
	}
}
