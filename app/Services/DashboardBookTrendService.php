<?php

namespace Tineidae\Services;

use Illuminate\Support\Carbon;
use Tineidae\Models\Read;

class DashboardBookTrendService extends DashboardTrendService {

	public function getValues(): array {
		return array_map(function(Carbon $month): string {
			return Read::whereMonth("read_at", $month->format("m"))
				->whereYear("read_at", $month->format("Y"))
				->count();
		}, $this->getMonthsArray());
	}

}
