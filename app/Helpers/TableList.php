<?php

namespace Tineidae\Helpers;

class TableList {

	protected $title = "";
	protected $dataset = [];
	protected $isSearchable = false;

	public function toArray(): array {
		return [
			"dataset" => $this->dataset,
			"title" => $this->title,
			"isSearchable" => $this->isSearchable,
		];
	}

	public function setDataset(array $dataset): self {
		$this->dataset = $dataset;
		return $this;
	}

	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}

	public function enableSearching(): self {
		$this->isSearchable = true;
		return $this;
	}

	public function eventually(bool $condition, $function): self {
		if($condition) {
			$function($this);
		}
		return $this;
	}

}
