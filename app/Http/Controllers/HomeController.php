<?php

namespace Tineidae\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Tineidae\Services\DashboardPageTrendService;
use Tineidae\Services\DashboardRecentReadDataService;
use Tineidae\Services\DashboardBookTrendService;

class HomeController extends Controller {

	const MONTH_LIMIT = 16;

	public function getRecentRead(): JsonResponse {
		/** @var DashboardRecentReadDataService $recentReadService */
		$recentReadService = app(DashboardRecentReadDataService::class);

		$this->response
			->setStatus(true)
			->setData($recentReadService->getData());

		return $this->getResponse();
	}

	public function getRecentReads(string $bookNumber): JsonResponse {
		/** @var DashboardRecentReadDataService $recentReadService */
		$recentReadService = app(DashboardRecentReadDataService::class);
		$recentReadService->setNumberOfBooks((int) $bookNumber);

		$reads = $recentReadService->getData();
		array_shift($reads);

		$this->response
			->setStatus(true)
			->setData($reads);

		return $this->getResponse();
	}

	public function getBookTrend(): JsonResponse {
		/** @var DashboardBookTrendService $chartService */
		$chartService = app(DashboardBookTrendService::class);
		$chartService->setMonthLimit(self::MONTH_LIMIT);

		$this->response
			->setStatus(true)
			->setData($chartService->getData());

		return $this->getResponse();
	}

	public function getPageTrend(): JsonResponse {
		/** @var DashboardPageTrendService $chartService */
		$chartService = app(DashboardPageTrendService::class);
		$chartService->setMonthLimit(self::MONTH_LIMIT);

		$this->response
			->setStatus(true)
			->setData($chartService->getData());

		return $this->getResponse();
	}

}
