<?php

namespace Tineidae\Http\Routes;

use Laravel\Lumen\Routing\Router;
use Tineidae\Interfaces\ApplicationRoutes;

abstract class Routes implements ApplicationRoutes {

	abstract public function __construct(Router $router);

}

