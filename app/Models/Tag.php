<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Tineidae\Models\Abstracts\StandardSystemModel;

/**
 * @property string name
 * @property string|null lastReadBookUuid
 * @property \Illuminate\Database\Eloquent\Collection books
 * @property Book|null book
 */
class Tag extends StandardSystemModel {

	protected $fillable = ["name", "last_read_book_uuid"];

	public function getSlugBase(): string {
		return $this->name;
	}

	public function books(): BelongsToMany {
		return $this->belongsToMany(Book::class, "book_tags");
	}

	public function book(): BelongsTo {
		return $this->belongsTo(Book::class, "last_read_book_uuid", "uuid");
	}

}
