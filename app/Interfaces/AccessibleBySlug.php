<?php

namespace Tineidae\Interfaces;

use Illuminate\Contracts\Support\Arrayable;

/**
 * @property string slug
 */
interface AccessibleBySlug {

	public function getBySlug(string $slug): Arrayable;
	public function getSlugBase(): string;

}
