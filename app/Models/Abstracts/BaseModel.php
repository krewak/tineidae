<?php

namespace Tineidae\Models\Abstracts;

use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UuidModel
 * @method static find(array $parameters)
 * @method static where(string $field, string $value)
 */
abstract class BaseModel extends Model {

	use PivotEventTrait;

	public static $snakeAttributes = false;

	public function toArray($camelCase = true): array {
		$array = parent::toArray();

		if(!$camelCase) {
			return $array;
		}

		$adjustedArray = [];

		foreach($array as $name => $value) {
			$adjustedArray[camel_case($name)] = $value;
		}

		return $adjustedArray;
	}

	public function getAttribute($key) {
		return parent::getAttribute(snake_case($key));
	}

}
