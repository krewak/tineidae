<?php

require_once __DIR__ . "/../vendor/autoload.php";

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Illuminate\Contracts\Console\Kernel as KernelContract;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Laravel\Lumen\Application;
use Tineidae\Console\Kernel;
use Tineidae\Exceptions\Handler;
use Tineidae\Http\Middleware\{
	Authenticated,
	Guest,
	HandleToken,
	RefreshToken
};
use Tineidae\Providers\{
	AppServiceProvider,
	DevelopmentEnvironmentServiceProvider,
	RepositoryInjectionServiceProvider,
	RouterServiceProvider
};

try {
	$env = new Dotenv(__DIR__ . "/../");
	$env->load();
} catch(InvalidPathException $e) {
	//
}

$app = new Application(realpath(__DIR__ . "/../"));

$app->withEloquent();

$app->singleton(ExceptionHandler::class, Handler::class);
$app->singleton(KernelContract::class, Kernel::class);

$app->routeMiddleware([
	"auth" => Authenticated::class,
	"guest" => Guest::class,
	"token" => HandleToken::class,
	"refresh" => RefreshToken::class,
]);

$app->register(AppServiceProvider::class);
$app->register(RepositoryInjectionServiceProvider::class);
$app->register(RouterServiceProvider::class);

if(app()->environment("local")) {
	$app->register(DevelopmentEnvironmentServiceProvider::class);
}

return $app;
