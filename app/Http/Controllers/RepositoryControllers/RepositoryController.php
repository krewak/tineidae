<?php

namespace Tineidae\Http\Controllers\RepositoryControllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tineidae\Http\Controllers\Controller;
use Tineidae\Interfaces\APIResponse;
use Tineidae\Interfaces\Repository;

abstract class RepositoryController extends Controller {

	protected $repository;

	/**
	 * Repository controller constructor.
	 * @param Request $request
	 * @param APIResponse $response
	 * @param Repository $repository
	 */
	public function __construct(Request $request, APIResponse $response, Repository $repository) {
		parent::__construct($request, $response);
		$this->repository = $repository;
	}

	/**
	 * @return JsonResponse
	 */
	public function getAll(): JsonResponse {
		$data = $this->repository->all();
		$this->response->setStatus(true)->setData($data);

		return $this->getResponse();
	}

	/**
	 * @param string $slug
	 * @return JsonResponse
	 */
	public function getBySlug(string $slug): JsonResponse {
		try {
			$data = $this->repository->get($slug);
			$this->response->setStatus(true)->setData($data);
		} catch(ModelNotFoundException $exception) {
			$this->response->pushMessage("Model doesn't exist.");
		}

		return $this->getResponse();
	}

}
