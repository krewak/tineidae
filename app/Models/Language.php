<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Tineidae\Models\Abstracts\StandardSystemModel;

/**
 * @property string name
 * @property string symbol
 * @property string abbreviation
 * @property \Illuminate\Database\Eloquent\Collection authors
 * @property \Illuminate\Database\Eloquent\Collection books
 * @property \Illuminate\Database\Eloquent\Collection reads
 */
class Language extends StandardSystemModel {

	protected $fillable = ["name", "symbol", "abbreviation"];

	public function getSlugBase(): string {
		return $this->name;
	}

	public function authors(): HasMany {
		return $this->hasMany(Author::class);
	}

	public function books(): HasMany {
		return $this->hasMany(Book::class);
	}

	public function reads(): HasMany {
		return $this->hasMany(Read::class);
	}

}
