<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Tineidae\Events\AuthorAssignedToBook;
use Tineidae\Events\BookCreated;
use Tineidae\Events\BookTagged;
use Tineidae\Events\StandardSystemModelCreated;
use Tineidae\Models\Abstracts\StandardSystemModel;

/**
 * @property string title
 * @property string original_title
 * @property Language language
 * @property string language_uuid
 * @property string publication_year
 * @property int pages
 * @property string cover
 * @property string summary
 * @property Read read
 */
class Book extends StandardSystemModel {

	protected $fillable = ["title", "original_title", "language_uuid", "publication_year", "pages", "cover", "summary"];
	protected $hidden = ["uuid", "language_uuid", "created_at", "updated_at"];
	protected $bag = [];

	public static function boot(): void {
		parent::boot();

		static::created(function(Book $book): void {
			event(new BookCreated($book));
		});

		static::pivotAttaching(function(Book $book, string $relationName, array $pivotIds): void {
			if($relationName === "tags") {
				event(new BookTagged($book, $pivotIds));
			}
			if($relationName === "authors") {
				event(new AuthorAssignedToBook($book, $pivotIds));
			}
		});
	}

	public function getSlugBase(): string {
		return $this->title;
	}

	public function read(): HasOne {
		return $this->hasOne(Read::class, "book_uuid");
	}

	public function authors(): BelongsToMany {
		return $this->belongsToMany(Author::class, "book_authors");
	}

	public function device(): BelongsTo {
		return $this->belongsTo(Device::class);
	}

	public function tags(): BelongsToMany {
		return $this->belongsToMany(Tag::class, "book_tags");
	}

	public function bookTags(): HasMany {
		return $this->hasMany(BookTag::class);
	}

}
