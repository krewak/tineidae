<?php

namespace Tineidae\Helpers;

class Gender {

	public $icon;
	public $name;

	const values = ["M", "F"];

	public function __construct(string $enumValue) {
		if($enumValue === "M") {
			$this->name = "mężczyzna";
			$this->icon = "mars";
		} else {
			$this->name = "kobieta";
			$this->icon = "venus";
		}
	}

}
