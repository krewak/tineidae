<?php

namespace Tineidae\Providers;

use Faker\Generator as FakerGenerator;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Tineidae\Helpers\APIResponse;
use Tineidae\Helpers\PolishMonth;
use Tineidae\Interfaces\APIResponse as APIResponseInterface;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register(): void {
		$this->app->singleton(FakerGenerator::class, function() {
			return FakerFactory::create("pl_PL");
		});

		$this->app->bind(APIResponseInterface::class, APIResponse::class);
	}

	public function boot(): void {
		Carbon::setLocale("pl");
		Carbon::macro("polishDate", function(): string {
			$words = [
				$this->format("j"),
				PolishMonth::get($this->format("n")),
				$this->format("Y"),
			];
			return implode(" ", $words);
		});
	}

}
