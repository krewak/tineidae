<?php

namespace Tineidae\Http\Routes;

use Laravel\Lumen\Routing\Router;

class GuestRoutes extends Routes {

	public function __construct(Router $router) {
		$router->post("/auth/login", ["as" => "auth.login", "uses" => "AuthController@authenticate"]);
	}

}

