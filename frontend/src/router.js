import Vue from "vue"
import Router from "vue-router"
import NotFound from "./components/System/NotFound"
import Index from "./components/Frontend/Index/Index"
import AuthorEntry from "./components/Frontend/Author/Entry"
import AuthorList from "./components/Frontend/Author/List"
import BookEntry from "./components/Frontend/Book/Entry"
import BookList from "./components/Frontend/Book/List"
import BookGallery from "./components/Frontend/Book/Gallery"
import LanguageEntry from "./components/Frontend/Language/Entry"
import LanguageList from "./components/Frontend/Language/List"
import TagEntry from "./components/Frontend/Tag/Entry"
import TagList from "./components/Frontend/Tag/List"
import Search from "./components/Frontend/Search/Search"

Vue.use(Router)

let routes = [
	{ path: "/", name: "index", component: Index, meta: { category: "system" } },

	{ path: "/ksiazki", name: "book.list", component: BookList, meta: { category: "books" } },
	{ path: "/ksiazki/galeria", name: "book.gallery", component: BookGallery, meta: { category: "books" } },
	{ path: "/ksiazki/:slug", name: "book.entry", component: BookEntry, meta: { category: "books" } },

	{ path: "/autorzy", name: "author.list", component: AuthorList, meta: { category: "authors" } },
	{ path: "/autorzy/:slug", name: "author.entry", component: AuthorEntry, meta: { category: "authors" } },

	{ path: "/jezyki", name: "language.list", component: LanguageList, meta: { category: "languages" } },
	{ path: "/jezyki/:slug", name: "language.entry", component: LanguageEntry, meta: { category: "languages" } },

	{ path: "/tagi", name: "tag.list", component: TagList, meta: { category: "tags" } },
	{ path: "/tagi/:slug", name: "tag.entry", component: TagEntry, meta: { category: "tags" } },

	{ path: "/nosniki", name: "device.list", component: BookList, meta: { category: "devices" } },
	{ path: "/nosniki/:slug", name: "device.entry", component: BookEntry, meta: { category: "devices" } },

	{ path: "/szukaj", name: "search", component: Search, meta: { category: "search" } },
	{ path: "/statystyki", name: "statistics", component: Index, meta: { category: "system" } },

	{ path: "*", name: "not-found", component: NotFound, meta: { category: "system" } },
]

const router = new Router({
	base: "/",
	mode: "history",
	routes: routes
})

export default router
