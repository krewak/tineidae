<?php

namespace Tineidae\Providers;

use Illuminate\Support\ServiceProvider;
use Tineidae\Http\Controllers\RepositoryControllers\Entries\AuthorController;
use Tineidae\Http\Controllers\RepositoryControllers\Entries\DeviceController;
use Tineidae\Http\Controllers\RepositoryControllers\Entries\LanguageController;
use Tineidae\Http\Controllers\RepositoryControllers\Entries\TagController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\AuthorsController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\BooksController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\DevicesController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\LanguageBooksController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\LanguagesController;
use Tineidae\Http\Controllers\RepositoryControllers\Lists\TagsController;
use Tineidae\Interfaces\Repository;
use Tineidae\Repositories\Author;
use Tineidae\Repositories\Device;
use Tineidae\Repositories\Language;
use Tineidae\Repositories\LanguageBook;
use Tineidae\Repositories\Tag;
use Tineidae\Repositories\Lists\Author as ListAuthor;
use Tineidae\Repositories\Lists\Book as ListBook;
use Tineidae\Repositories\Lists\Language as ListLanguage;
use Tineidae\Repositories\Lists\Tag as ListTag;

class RepositoryInjectionServiceProvider extends ServiceProvider {

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register(): void {
		$this->buildInjection(AuthorController::class, new Author());
		$this->buildInjection(LanguageController::class, new Language());
		$this->buildInjection(DeviceController::class, new Device());
		$this->buildInjection(TagController::class, new Tag());

		$this->buildInjection(AuthorsController::class, new ListAuthor());
		$this->buildInjection(BooksController::class, new ListBook());
		$this->buildInjection(LanguagesController::class, new ListLanguage());
		$this->buildInjection(DevicesController::class, new Device());
		$this->buildInjection(TagsController::class, new ListTag());

		$this->buildInjection(LanguageBooksController::class, new LanguageBook());
	}

	/**
	 * Simplified registering interface binding to dependency injection for repository controllers
	 *
	 * @param string $controllerClass
	 * @param Repository $repository
	 * @param string $interface
	 */
	protected function buildInjection(string $controllerClass, Repository $repository, string $interface = Repository::class): void {
		$this->app->when($controllerClass)
			->needs($interface)
			->give(function() use ($repository): Repository {
				return $repository;
			});
	}

}
