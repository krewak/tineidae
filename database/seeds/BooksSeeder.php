<?php

use Illuminate\Database\Seeder;
use Tineidae\Models\Author;
use Tineidae\Models\Book;
use Tineidae\Models\Language;
use Tineidae\Models\Read;
use Tineidae\Models\Tag;

class BooksSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(): void {
		factory(Language::class, 5)->create()->each(function(Language $language) {
			factory(Author::class, rand(0, 5))->create([
				"language_uuid" => $language->uuid
			])->each(function(Author $author) {
				factory(Book::class, rand(0, 5))->create([
					"language_uuid" => $author->language_uuid,
				]);
			});

		});

		factory(Tag::class, 25)->create();

		$authors = Author::pluck("uuid");
		$languages = Language::pluck("uuid");
		$tags = Tag::pluck("uuid");

		foreach(Read::all() as $read) {
			$fake = factory(Read::class)->make()->toArray(false);
			$read->fill($fake);
			$read->language_uuid = $languages->random();
			$read->save();

			$limit = array_rand([1, 1, 1, 2]);
			for($i = 0; $i < $limit; $i++) {
				try {
					$read->book->authors()->attach($authors->random());
				} catch(Exception $exception) {}
			}

			$limit = array_rand([4, 5, 6, 7, 8, 9]);
			for($i = 0; $i < $limit; $i++) {
				try {
					$read->book->tags()->attach($tags->random());
				} catch(Exception $exception) {}
			}
		}

	}

}
