<?php

namespace Tineidae\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tineidae\Services\JSONWebTokenService;

class RefreshToken extends Middleware {

	public function handle(Request $request, Closure $next, $guard = null) {
		if($request["token"]) {
			$service = app(JSONWebTokenService::class);
			$request["token"] = $service->extendTokenExpirationDate($request["token"]);
		}

		return $next($request);
	}

}
