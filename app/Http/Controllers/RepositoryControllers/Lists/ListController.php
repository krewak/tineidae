<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tineidae\Http\Controllers\RepositoryControllers\RepositoryController;
use Tineidae\Interfaces\Repository;

abstract class ListController extends RepositoryController {

	protected function getSearchableColumns(): array { return []; }
	protected function getSortableColumns(): array { return []; }

	/**
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function get(Request $request): JsonResponse {
		$repository = $this->repository;

		$searchedPhrase = $request->get("searchPhrase");
		if($this->getSearchableColumns() && $searchedPhrase) {
			$repository = $repository->filter($this->getSearchableColumns(), $searchedPhrase);
		}

		$sortingColumn = $request->get("sortBy");
		$orderByDescending = $request->get("orderBy") === "descending";
		if($sortingColumn && in_array($sortingColumn, array_keys($this->getSortableColumns()))) {
			$repository = $repository->sort($this->getSortableColumns()[$sortingColumn], !$orderByDescending);
		}

		$repository = $this->customizeRepository($repository);
		$data = $repository->all();

		$this->response
			->setStatus(true)
			->setData($data);

		return $this->getResponse();
	}

	protected function customizeRepository(Repository $repository): Repository {
		return $repository;
	}

}
