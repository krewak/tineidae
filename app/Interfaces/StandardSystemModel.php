<?php

namespace Tineidae\Interfaces;

 /**
  * @property string uuid
  */
interface StandardSystemModel extends AccessibleBySlug, IdentifiableByUUID {

}
