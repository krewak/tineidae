<?php

namespace Tineidae\Services;

use Illuminate\Support\Carbon;
use Tineidae\Helpers\NumberToRoman;

abstract class DashboardTrendService {

	private $monthLimit = 6;

	abstract protected function getValues(): array;

	public function getData(): array {
		return [
			"labels" => $this->getLabels(),
			"values" => $this->getValues(),
		];
	}

	public function setMonthLimit(int $newMonthLimit): self {
		$this->monthLimit = $newMonthLimit;
		return $this;
	}

	protected function getLabels(): array {
		return array_map(function(Carbon $date): string {
			$month = NumberToRoman::transform($date->format("m"));
			$year = $date->format("Y");
			return  "$month $year";
		}, $this->getMonthsArray());
	}

	protected function getMonthsArray(): array {
		$months = [];
		$month = Carbon::now()->startOfMonth();

		for($i = 0; $i <= $this->monthLimit; $i++) {
			$months[] = $month;
			$month = $month->copy()->subMonth();
		}

		return array_reverse($months);
	}

}
