<?php

namespace Tineidae\Models;

use Tineidae\Interfaces\Authenticatable;
use Tineidae\Models\Abstracts\UuidModel;

/**
 * Class User
 * @package Tineidae\Models
 * @property string uuid
 * @property string email
 * @property string name
 * @property string password
 */
class User extends UuidModel implements Authenticatable {

	protected $fillable = ["email", "name"];
	protected $hidden = ["uuid", "password"];

	public function getID(): string {
		return (string) $this->uuid;
	}

}
