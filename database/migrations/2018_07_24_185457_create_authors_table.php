<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void {
		Schema::create("authors", function(Blueprint $table) {
			$table->string("uuid")->unique();
			$table->string("slug")->unique();

			$table->string("first_name")->default("");
			$table->string("last_name")->default("");
			$table->string("alt_name")->default("");

			$table->enum("gender", ["M", "F"]);
			$table->integer("birth_year")->nullable()->default(null);
			$table->integer("death_year")->nullable()->default(null);

			$table->string("language_uuid");
			$table->foreign("language_uuid")->references("uuid")->on("languages");

			$table->string("portrait")->default("");

			$table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
			$table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void {
		Schema::table("authors", function(Blueprint $table) {
			$table->dropForeign(["language_uuid"]);
		});
		Schema::dropIfExists("authors");
	}
}
