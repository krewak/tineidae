'use strict'

const path = require('path')

module.exports = {
	dev: {
		assetsSubDirectory: 'static',
		assetsPublicPath: '/',
		host: 'localhost',
		port: 7224,
		autoOpenBrowser: false,
		errorOverlay: true,
		notifyOnErrors: true,
		poll: false,
		useEslint: false,
		showEslintErrorsInOverlay: false,
		devtool: 'cheap-module-eval-source-map',
		cacheBusting: true,
		cssSourceMap: true,
		proxyTable: {
			"/api": {
				target: "http://tineidae.local",
				changeOrigin: true,
				partRewrite: {
					"^/": ""
				}
			}
		}
	},

	build: {
		index: path.resolve(__dirname, '../dist/index.html'),
		assetsRoot: path.resolve(__dirname, '../dist'),
		assetsSubDirectory: 'static',
		assetsPublicPath: '/',
		productionSourceMap: true,
		devtool: '#source-map',
		productionGzip: false,
		productionGzipExtensions: ['js', 'css'],
		bundleAnalyzerReport: process.env.npm_config_report
	}
}
