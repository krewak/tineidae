<?php

namespace Tineidae\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Tineidae\Models\Abstracts\BaseModel;

/**
 * @property Book $book
 * @property string $book_uuid
 * @property Tag $tag
 * @property string $tag_uuid
 */
class BookTag extends BaseModel {

	protected $fillable = ["book_uuid", "tag_uuid"];
	protected $table = "book_tags";

	public function book(): BelongsTo {
		return $this->belongsTo(Book::class);
	}

	public function tag(): BelongsTo {
		return $this->belongsTo(Tag::class);
	}

}
