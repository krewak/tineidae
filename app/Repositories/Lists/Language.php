<?php

namespace Tineidae\Repositories\Lists;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Repositories\Language as BaseLanguageRepository;

class Language extends BaseLanguageRepository {

	protected function extendQuery(Builder $query): Builder {
		return $query
			->withCount("authors")
			->withCount("reads")
			->withCount("books")
			->orderBy("languages.name");
	}

	protected function map(Arrayable $model): array {
		/** @var \Tineidae\Models\Language $model */
		$array = $model->toArray();
		return $array;
	}

}
