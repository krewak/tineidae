<?php

namespace Tineidae\Repositories\Lists;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Book as BookModel;
use Tineidae\Repositories\ModelRepository;

class Book extends ModelRepository {

	protected function getDataSource(): AccessibleBySlug {
		return new BookModel();
	}

	protected function extendQuery(Builder $query): Builder {
		return $query
			->join("reads", "reads.book_uuid", "=", "books.uuid")
			->orderByRaw("reads.read_at IS NOT NULL, reads.read_at DESC, books.created_at DESC");
	}

	protected function map(Arrayable $model): array {
		/** @var \Tineidae\Models\Book $model */
		$array = $model->toArray();
		return $array;
	}

}
