import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

const state = {
	tables: {
		authors: null,
		books: null,
		tags: null,
		languages: null,
	},
	user: null,
}

const getters = {
	authors: state => state.tables.authors,
	books: state => state.tables.books,
	tags: state => state.tables.tags,
	languages: state => state.tables.languages,
	user: state => state.user,
}

const mutations = {
	cacheTable(state, cache) {
		state.tables[cache.name] = cache.data
	}
}

export default new Vuex.Store({
	state,
	getters,
	mutations
})
