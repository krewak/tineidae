<?php

namespace Tineidae\Repositories;

use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Models\Device as DeviceModel;

class Device extends ModelRepository {

	protected function getDataSource(): AccessibleBySlug {
		return new DeviceModel();
	}

}