<?php

namespace Tineidae\Http\Controllers\RepositoryControllers\Lists;

class BooksController extends ListController {

	protected function getSearchableColumns(): array {
		return [
			"title" => "title",
		];
	}

	protected function getSortableColumns(): array {
		return [
		];
	}

}
