<?php

namespace Tineidae\Events;

use Tineidae\Interfaces\AccessibleBySlug;

class GenerateModelSlug {

	public function __construct(AccessibleBySlug $model) {
		$slug = str_slug($model->getSlugBase());
		$slug = $this->buildUniqueSlug($model, $slug);

		$model->slug = $slug;
	}

	private function buildUniqueSlug(AccessibleBySlug $model, string $slug): string {
		$count = $model::select("slug")
			->where("slug", "LIKE", "$slug%")
			->count();

		if($count) {
			$slug = str_slug("$slug ++$count");
		}

		return $slug;
	}

}