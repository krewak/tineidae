<?php

namespace Tineidae\Repositories;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Tineidae\Interfaces\AccessibleBySlug;
use Tineidae\Interfaces\Repository;

abstract class ModelRepository implements Repository {

	protected $filters = [];
	protected $masterFilters = [];
	protected $sortingColumn = "";
	protected $sortingAscending = true;

	abstract protected function getDataSource(): AccessibleBySlug;

	/**
	 * Allows extend model query
	 * @param Builder $query
	 * @return Builder
	 */
	protected function extendQuery(Builder $query): Builder {
		return $query;
	}

	/**
	 * Maps item into array for response
	 * @param Arrayable $model
	 * @return array
	 */
	protected function map(Arrayable $model): array {
		return $model->toArray();
	}

	/**
	 * Retrieves all items from repository
	 * @return array
	 */
	public function all(): array {
		/** @var Builder $query */
		$query = $this->getDataSource()::query();

		if($this->masterFilters) {
			foreach($this->masterFilters as $column => $value) {
				$query = $query->where($column, "=", $value);
			}
		}

		if($this->filters) {
			$filters = $this->filters;
			$query = $query->where(function($query) use($filters) {
				foreach($filters as $column => $phrase) {
					$query = $query->orWhere($column, "LIKE", "%" . $phrase . "%");
				}
			});
		}

		if($this->sortingColumn) {
			$query = $query->orderByRaw($this->sortingColumn . " IS NULL")
				->orderBy($this->sortingColumn, $this->sortingAscending ? "asc" : "desc");
		}

		$query = $this->extendQuery($query);
		$data = $query->get();

		$array = [];
		foreach($data as $model) {
			array_push($array, $this->map($model));
		}

		return $array;
	}

	/**
	 * Retrieves item from repository by slug
	 * @param string $slug
	 * @return array
	 */
	public function get(string $slug = ""): array {
		$data = $this->getDataSource()->getBySlug($slug);
		return $this->map($data);
	}

	/**
	 * Builds filters set for query
	 * @param string $column
	 * @param string $phrase
	 * @return Repository
	 */
	public function addMasterFilter(string $column, string $phrase = ""): Repository {
		$this->masterFilters[$column] = $phrase;
		return $this;
	}

	/**
	 * Builds filters set for query
	 * @param array $columns
	 * @param string $phrase
	 * @return Repository
	 */
	public function filter(array $columns, string $phrase = ""): Repository {
		foreach($columns as $column) {
			$this->filters[$column] = $phrase;
		}
		return $this;
	}

	/**
	 * Builds sorting for query
	 * @param string $column
	 * @param bool $ascending
	 * @return Repository
	 */
	public function sort(string $column, bool $ascending = true): Repository {
		$this->sortingColumn = $column;
		$this->sortingAscending = $ascending;
		return $this;
	}

}