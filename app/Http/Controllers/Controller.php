<?php

namespace Tineidae\Http\Controllers;

use Illuminate\Log\Logger;
use Tineidae\Interfaces\APIResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Psr\Log\LoggerInterface;

abstract class Controller extends BaseController {

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var APIResponse
	 */
	protected $response;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * Controller constructor.
	 * @param Request $request
	 * @param APIResponse $response
	 */
	public function __construct(Request $request, APIResponse $response) {
		$this->request = $request;
		$this->response = $response;
		$this->logger = app(Logger::class);

		if(!empty($request->auth)) {
			$this->response->setUser($request->auth);
		}

		if(!empty($request->token)) {
			$this->response->setToken($request->token);
		}
	}

	/**
	 * @param int $status
	 * @return JsonResponse
	 */
	protected function getResponse(int $status = JSONResponse::HTTP_OK): JSONResponse {
		return response()->json($this->response->get(), $status);
	}

}
